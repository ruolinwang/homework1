package ejm.admin.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import ejm.admin.model.enums.CarType;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Objects;

/**
 * @author Ruolin Wang
 */
@Entity
@Table(name = "car")
@NamedQueries({
        @NamedQuery(name = "Car.findAll", query = "SELECT c from Car c"),
        @NamedQuery(name = "Car.findAllByPersonId", query = "SELECT c from Car c join c.owner o where o.id = :personId")
})
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope = Car.class)
public class Car {
    @Id
    @SequenceGenerator(
            name = "car_sequence",
            allocationSize = 1,
            initialValue = 2000
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car_sequence")
    @Column(name = "id", updatable = false, nullable = false)
    protected Integer id;

    @NotNull
    @Size(min = 3, max = 50)
    @Column(name = "brand", length = 50, nullable = false)
    protected String brand;

    @NotNull
    @Enumerated(EnumType.STRING)
    protected CarType type;

    @NotNull
    @Size(min = 7, max = 7)
    @Column(name = "license", length = 7, nullable = false)
    protected String license;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "person_id")
    protected Person owner;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public CarType getType() {
        return type;
    }

    public void setType(CarType type) {
        this.type = type;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public Person getOwner() {
        return owner;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(id, car.id) &&
                Objects.equals(brand, car.brand) &&
                type == car.type &&
                Objects.equals(license, car.license) &&
                Objects.equals(owner, car.owner);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, brand, type, license, owner);
    }
}
