package ejm.admin.model.enums;



public enum CarType  {

    SUV("SUV"),
    TRUCK("Truck"),
    SEDAN("Sedan"),
    VAN("Van"),
    COUPLE("Couple"),
    SPORTS("Sports");


    private final String label;


    CarType(String label) {
        this.label = label;
    }


    public String getLabel() {
        return label;
    }

}
