package ejm.admin;


import ejm.admin.model.Person;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.wildfly.swarm.arquillian.DefaultDeployment;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Ken Finnigan
 */
@RunWith(Arquillian.class)
@DefaultDeployment
@RunAsClient
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PersonResourceTest {

    @BeforeClass
    public static void setup() throws Exception {
        RestAssured.baseURI = "http://localhost:8080";
    }

    @Test
    public void aRetrieveAllPersons() throws Exception {
        Response response =
                when()
                        .get("/admin/person")
                .then()
                        .extract().response();

        String jsonAsString = response.asString();
        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");

        assertThat(jsonAsList.size()).isEqualTo(10);

        Map<String, ?> record1 = jsonAsList.get(0);

        assertThat(record1.get("id")).isEqualTo(0);
        assertThat(record1.get("age")).isEqualTo(21);
        assertThat(record1.get("name")).isEqualTo("Tom");
        assertThat(record1.get("email")).isEqualTo("tom@gmail.com");

        Map<String, ?> record2 = jsonAsList.get(9);

        assertThat(record2.get("id")).isEqualTo(9);
        assertThat(record2.get("age")).isEqualTo(22);
        assertThat(record2.get("name")).isEqualTo("Wang");
        assertThat(record2.get("email")).isEqualTo("wang@gmail.com");

    }

    @Test
    public void bRetrievePerson() throws Exception {
        Response response =
                given()
                        .pathParam("personId", 9)
                .when()
                        .get("/admin/person/{personId}")
                .then()
                        .extract().response();

        String jsonAsString = response.asString();

        Person person = JsonPath.from(jsonAsString).getObject("", Person.class);

        assertThat(person.getId()).isEqualTo(9);
        assertThat(person.getAge()).isEqualTo(22);
        assertThat(person.getName()).isEqualTo("Wang");
        assertThat(person.getEmail()).isEqualTo("wang@gmail.com");
    }

    @Test
    public void cCreatePerson() throws Exception {
        Person person = new Person();
        person.setName("TTT");
        person.setAge(22);
        person.setEmail("ttt@gmail.com");

        Response response =
                given()
                        .contentType(ContentType.JSON)
                        .body(person)
                .when()
                        .post("/admin/person");

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(201);
        String locationUrl = response.getHeader("Location");
        Integer personId = Integer.valueOf(locationUrl.substring(locationUrl.lastIndexOf('/') + 1));

        response =
                when()
                        .get("/admin/person")
                .then()
                        .extract().response();

        String jsonAsString = response.asString();
        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");

        assertThat(jsonAsList.size()).isEqualTo(11);

        response =
                given()
                        .pathParam("personId", personId)
                .when()
                        .get("/admin/person/{personId}")
                .then()
                        .extract().response();

        jsonAsString = response.asString();

        Person readBackPerson = JsonPath.from(jsonAsString).getObject("", Person.class);

        assertThat(readBackPerson.getId()).isEqualTo(personId);
        assertThat(readBackPerson.getAge()).isEqualTo(22);
        assertThat(readBackPerson.getName()).isEqualTo("TTT");
        assertThat(readBackPerson.getEmail()).isEqualTo("ttt@gmail.com");
    }

    @Test
    public void dFailToCreatePersonFromNullName() throws Exception {
        Person badPerson = new Person();
        badPerson.setAge(22);
        badPerson.setEmail("ttt@gmail.com");
        Response response =
                given()
                        .contentType(ContentType.JSON)
                        .body(badPerson)
                .when()
                        .post("/admin/person");

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(400);
        assertThat(response.getBody().asString()).contains("Validation failed for classes [ejm.admin.model.Person] during persist time for groups [javax.validation.groups.Default, ]\n" +
                                                                   "List of constraint violations:[\n" +
                                                                   "\tConstraintViolationImpl{interpolatedMessage='must not be null', propertyPath=name, rootBeanClass=class ejm.admin.model.Person, messageTemplate='{javax.validation.constraints.NotNull.message}'}\n" +
                                                                   "]");

        response =
                when()
                        .get("/admin/person")
                .then()
                        .extract().response();

        String jsonAsString = response.asString();
        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");

        assertThat(jsonAsList.size()).isEqualTo(11);
    }

    @Test
    public void eUpdatePerson() throws Exception {
        Response response =
                given()
                        .pathParam("personId", 9)
                        .when()
                        .get("/admin/person/{personId}")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();

        Person person = JsonPath.from(jsonAsString).getObject("", Person.class);

        assertThat(person.getId()).isEqualTo(9);
        assertThat(person.getName()).isEqualTo("Wang");

        //update the person name
        person.setName("Ruolin");
        response =
                given()
                        .contentType(ContentType.JSON)
                        .body(person)
                        .pathParam("personId", 9)
                        .when()
                        .put("/admin/person/{personId}")
                        .then()
                        .extract().response();
        jsonAsString = response.asString();
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(200);

        // check the return from update request
        Person retPerson = JsonPath.from(jsonAsString).getObject("", Person.class);
        assertThat(retPerson.getName()).isEqualTo("Ruolin");

        // check by reading again the person
        response =
                given()
                        .pathParam("personId", 9)
                        .when()
                        .get("/admin/person/{personId}")
                        .then()
                        .extract().response();

        jsonAsString = response.asString();

        person = JsonPath.from(jsonAsString).getObject("", Person.class);
        assertThat(person.getName()).isEqualTo("Ruolin");

    }



    @Test
    public void gDeletePerson() throws Exception {
        Response response =
                given()
                        .pathParam("personId", 9)
                        .when()
                        .delete("/admin/person/{personId}")
                        .then()
                        .extract().response();
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(204);
        response =
                given()
                        .pathParam("personId", 9)
                        .when()
                        .get("/admin/person/{personId}")
                        .then()
                        .extract().response();

        assertThat(response).isNotNull();
        // 204 means there is no person returned
        assertThat(response.getStatusCode()).isEqualTo(204);

        // delete entity not existed in database
        response =
                given()
                        .pathParam("personId", 2014)
                        .when()
                        .delete("/admin/person/{personId}")
                        .then()
                        .extract().response();
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(500);
        assertThat(response.getBody().asString()).contains("attempt to create delete event with null entity");

    }
}
