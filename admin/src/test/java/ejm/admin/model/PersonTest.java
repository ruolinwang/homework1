package ejm.admin.model;

import org.fest.assertions.Assertions;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Ken Finnigan
 */
public class PersonTest {

    @Test
    public void personsAreEqual() {

        Person person1 = createPerson(1, "Tom", "tom@gmail.com", 21);
        Person person2 = createPerson(1, "Tom", "tom@gmail.com", 21);

        Assertions.assertThat(person1).isEqualTo(person2);
        assertThat(person1.equals(person2)).isTrue();
        assertThat(person1.hashCode()).isEqualTo(person2.hashCode());
    }

    @Test
    public void personsAreNotEqual() {
        Person person1 = createPerson(2, "Tom", "tom@gmail.com", 21);
        Person person2 = createPerson(1, "Tom", "tom@gmail.com", 21);

        Assertions.assertThat(person1).isNotEqualTo(person2);
        assertThat(person1.equals(person2)).isFalse();
        assertThat(person1.hashCode()).isNotEqualTo(person2.hashCode());
    }

    @Test
    public void personModification() {
        Person person1 = createPerson(1, "Tom", "tom@gmail.com", 21);
        Person person2 = createPerson(1, "Tom", "tom@gmail.com", 21);

        Assertions.assertThat(person1).isEqualTo(person2);
        assertThat(person1.equals(person2)).isTrue();
        assertThat(person1.hashCode()).isEqualTo(person2.hashCode());

        person1.setAge(22);

        Assertions.assertThat(person1).isNotEqualTo(person2);
        assertThat(person1.equals(person2)).isFalse();
        assertThat(person1.hashCode()).isNotEqualTo(person2.hashCode());
    }


    private Person createPerson(Integer id, String name, String email, Integer age) {
        return new TestPersonObject(id, name, email, age);
    }
}
