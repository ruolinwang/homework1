package ejm.admin.model;

import ejm.admin.model.enums.CarType;
import org.fest.assertions.Assertions;
import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Ken Finnigan
 */
public class CarTest {

    @Test
    public void carsAreEqual() {

        Person owner = createPerson(1, "Tom", "tom@gmail.com", 21);
        Car car1 = createCar(1, "Toyota", CarType.COUPLE, "TXV2525", owner);
        Car car2 = createCar(1, "Toyota", CarType.COUPLE, "TXV2525", owner);

        Assertions.assertThat(car1).isEqualTo(car2);
        assertThat(car1.equals(car2)).isTrue();
        assertThat(car1.hashCode()).isEqualTo(car2.hashCode());
    }

    @Test
    public void carsAreNotEqual() {
        Person owner = createPerson(1, "Tom", "tom@gmail.com", 21);
        Car car1 = createCar(2, "Toyota", CarType.COUPLE, "TXV2525", owner);
        Car car2 = createCar(1, "Toyota", CarType.COUPLE, "TXV2525", owner);


        Assertions.assertThat(car1).isNotEqualTo(car2);
        assertThat(car1.equals(car2)).isFalse();
        assertThat(car1.hashCode()).isNotEqualTo(car2.hashCode());
    }

    @Test
    public void carModification() {
        Person owner = createPerson(1, "Tom", "tom@gmail.com", 21);
        Car car1 = createCar(1, "Toyota", CarType.COUPLE, "TXV2525", owner);
        Car car2 = createCar(1, "Toyota", CarType.COUPLE, "TXV2525", owner);

        Assertions.assertThat(car1).isEqualTo(car2);
        assertThat(car1.equals(car2)).isTrue();
        assertThat(car1.hashCode()).isEqualTo(car2.hashCode());

        car1.setType(CarType.SEDAN);

        Assertions.assertThat(car1).isNotEqualTo(car2);
        assertThat(car1.equals(car2)).isFalse();
        assertThat(car1.hashCode()).isNotEqualTo(car2.hashCode());
    }

    @Test
    public void categoriesWithIdenticalOwnerAreEqual() throws Exception {
        Person owner = createPerson(1, "Tom", "tom@gmail.com", 21);
        Car car1 = createCar(1, "Toyota", CarType.COUPLE, "TXV2525", owner);
        Car car2 = createCar(1, "Toyota", CarType.COUPLE, "TXV2525", owner);

        Assertions.assertThat(car1).isEqualTo(car2);
        assertThat(car1.equals(car2)).isTrue();
        assertThat(car1.hashCode()).isEqualTo(car2.hashCode());
    }

    @Test
    public void categoriesWithDifferentOwnerAreEqual() throws Exception {
        Person owner1 = createPerson(1, "Tom1", "tom1@gmail.com", 21);
        Person owner2 = createPerson(1, "Tom2", "tom2@gmail.com", 21);
        Car car1 = createCar(2, "Toyota", CarType.COUPLE, "TXV2525", owner1);
        Car car2 = createCar(1, "Toyota", CarType.COUPLE, "TXV2525", owner2);

        Assertions.assertThat(car1).isNotEqualTo(car2);
        assertThat(car1.equals(car2)).isFalse();
        assertThat(car1.hashCode()).isNotEqualTo(car2.hashCode());
    }


    private Car createCar(Integer id, String brand, CarType type, String license, Person owner) {
        return new TestCarObject(id, brand, type, license, owner);
    }

    private Person createPerson(Integer id, String name, String email, Integer age) {
        return new TestPersonObject(id, name, email, age);
    }
}
