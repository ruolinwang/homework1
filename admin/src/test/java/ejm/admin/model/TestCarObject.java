package ejm.admin.model;


import ejm.admin.model.enums.CarType;

/**
 * @author Ruolin Wang
 */
public class TestCarObject extends Car {
    public TestCarObject(Integer id) {
        this.id = id;
    }

    public TestCarObject() {
    }

    TestCarObject(Integer id,
                   String brand,
                   CarType type,
                   String license,
                   Person owner
    ) {
        this.id = id;
        this.brand = brand;
        this.type = type;
        this.license = license;
        this.owner = owner;
    }


}
