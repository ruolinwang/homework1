package ejm.admin;


import ejm.admin.model.Car;
import ejm.admin.model.Person;
import ejm.admin.model.enums.CarType;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.wildfly.swarm.arquillian.DefaultDeployment;

import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.fest.assertions.Assertions.assertThat;

/**
 * @author Ken Finnigan
 */
@RunWith(Arquillian.class)
@DefaultDeployment
@RunAsClient
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CarResourceTest {

    @BeforeClass
    public static void setup() throws Exception {
        RestAssured.baseURI = "http://localhost:8080";
    }

    @Test
    public void aRetrieveAllCars() throws Exception {
        Response response =
                when()
                        .get("/admin/car")
                .then()
                        .extract().response();

        String jsonAsString = response.asString();
        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");

        assertThat(jsonAsList.size()).isEqualTo(5);

        Map<String, ?> record1 = jsonAsList.get(0);

        assertThat(record1.get("id")).isEqualTo(0);
        assertThat(record1.get("brand")).isEqualTo("Toyota");
        assertThat(record1.get("type")).isEqualTo(CarType.COUPLE.toString());
        assertThat(record1.get("license")).isEqualTo("aaa1234");
        assertThat(record1.get("owner").toString()).isEqualTo("{name=Tom, id=0, email=tom@gmail.com, age=21}");


        Map<String, ?> record2 = jsonAsList.get(4);

        assertThat(record2.get("id")).isEqualTo(4);
        assertThat(record2.get("brand")).isEqualTo("Toyota");
        assertThat(record2.get("type")).isEqualTo(CarType.SEDAN.toString());
        assertThat(record2.get("license")).isEqualTo("eee1234");
        assertThat(record2.get("owner").toString()).isEqualTo("{name=DDD, id=4, email=ddd@gmail.com, age=21}");

    }

    @Test
    public void bRetrieveCar() throws Exception {
        Response response =
                given()
                        .pathParam("carId", 4)
                .when()
                        .get("/admin/car/{carId}")
                .then()
                        .extract().response();

        String jsonAsString = response.asString();
        System.out.println(jsonAsString);

        Car car = JsonPath.from(jsonAsString).getObject("", Car.class);

        assertThat(car.getId()).isEqualTo(4);
        assertThat(car.getBrand()).isEqualTo("Toyota");
        assertThat(car.getType()).isEqualTo(CarType.SEDAN);
        assertThat(car.getLicense()).isEqualTo("eee1234");
        assertThat(car.getOwner().getId()).isEqualTo(4);
    }

    @Test
    public void bRetrieveCarsByPerson() throws Exception {
        Response response =
                given()
                        .pathParam("personId", 3)
                        .when()
                        .get("/admin/car/person/{personId}")
                        .then()
                        .extract().response();



        String jsonAsString = response.asString();
        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");

        assertThat(jsonAsList.size()).isEqualTo(2);

        Map<String, ?> record1 = jsonAsList.get(0);

        assertThat(record1.get("id")).isEqualTo(1);
        assertThat(record1.get("brand")).isEqualTo("Toyota");
        assertThat(record1.get("type")).isEqualTo(CarType.COUPLE.toString());
        assertThat(record1.get("license")).isEqualTo("bbb1234");
        assertThat(record1.get("owner").toString()).isEqualTo("{name=CCC, id=3, email=ccc@gmail.com, age=21}");


    }

    @Test
    public void cCreateCar() throws Exception {
        Car car = new Car();
        car.setBrand("Honda");
        car.setType(CarType.SEDAN);
        car.setLicense("ttt4567");
        Response response =
                given()
                        .pathParam("personId", 9)
                        .when()
                        .get("/admin/person/{personId}")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();

        Person person = JsonPath.from(jsonAsString).getObject("", Person.class);
        car.setOwner(person);

        response =
                given()
                        .contentType(ContentType.JSON)
                        .body(car)
                .when()
                        .post("/admin/car");

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(201);
        String locationUrl = response.getHeader("Location");
        Integer carId = Integer.valueOf(locationUrl.substring(locationUrl.lastIndexOf('/') + 1));

        response =
                when()
                        .get("/admin/car")
                .then()
                        .extract().response();

        jsonAsString = response.asString();
        List<Map<String, ?>> jsonAsList = JsonPath.from(jsonAsString).getList("");

        assertThat(jsonAsList.size()).isEqualTo(6);

        response =
                given()
                        .pathParam("carId", carId)
                .when()
                        .get("/admin/car/{carId}")
                .then()
                        .extract().response();

        jsonAsString = response.asString();

        Car readBackCar = JsonPath.from(jsonAsString).getObject("", Car.class);

        assertThat(readBackCar.getId()).isEqualTo(carId);
        assertThat(readBackCar.getBrand()).isEqualTo("Honda");
        assertThat(readBackCar.getType()).isEqualTo(CarType.SEDAN);
        assertThat(readBackCar.getLicense()).isEqualTo("ttt4567");
        assertThat(readBackCar.getOwner().getId()).isEqualTo(9);
    }



    @Test
    public void eUpdateCar() throws Exception {
        Response response =
                given()
                        .pathParam("carId", 2)
                        .when()
                        .get("/admin/car/{carId}")
                        .then()
                        .extract().response();

        String jsonAsString = response.asString();

        Car car = JsonPath.from(jsonAsString).getObject("", Car.class);

        assertThat(car.getId()).isEqualTo(2);
        assertThat(car.getBrand()).isEqualTo("Toyota");

        //update the person name
        car.setBrand("Honda");
        response =
                given()
                        .contentType(ContentType.JSON)
                        .body(car)
                        .pathParam("carId", 2)
                        .when()
                        .put("/admin/car/{carId}")
                        .then()
                        .extract().response();
        jsonAsString = response.asString();
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(200);

        // check the return from update request
        Car retCar = JsonPath.from(jsonAsString).getObject("", Car.class);
        assertThat(retCar.getBrand()).isEqualTo("Honda");

        // check by reading again the person
        response =
                given()
                        .pathParam("carId", 2)
                        .when()
                        .get("/admin/car/{carId}")
                        .then()
                        .extract().response();

        jsonAsString = response.asString();

        car = JsonPath.from(jsonAsString).getObject("", Car.class);
        assertThat(car.getBrand()).isEqualTo("Honda");

    }



    @Test
    public void gDeleteCar() throws Exception {
        Response response =
                given()
                        .pathParam("carId", 2)
                        .when()
                        .delete("/admin/car/{carId}")
                        .then()
                        .extract().response();
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(204);
        response =
                given()
                        .pathParam("carId", 2)
                        .when()
                        .get("/admin/car/{carId}")
                        .then()
                        .extract().response();

        assertThat(response).isNotNull();
        // 204 means there is no car returned
        assertThat(response.getStatusCode()).isEqualTo(204);

        // delete entity not existed in database
        response =
                given()
                        .pathParam("carId", 2014)
                        .when()
                        .delete("/admin/car/{carId}")
                        .then()
                        .extract().response();
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(500);
        assertThat(response.getBody().asString()).contains("attempt to create delete event with null entity");

    }
}
